/*! Image Zoooom - v0.1.0 - 2015-12-17
 * Copyright (c) 2015 Diana Burduja; Licensed MIT license */
jQuery(document).ready(function(a) {
    function b() {
        var b = {},
            e = {
                lensShape: a("input[name=lensShape]:checked").val(),
                cursorType: a("input[name=cursorType]:checked").val(),
                zwEasing: parseInt(a("#zwEasing").val()),
                lensSize: parseInt(a("#lensSize").val()),
                lensColour: a("#lensColour").val(),
                lensOverlay: a("#lensOverlay").is(":checked"),
                borderThickness: parseInt(a("#borderThickness").val()),
                borderColor: a("#borderColor").val(),
                borderRadius: parseInt(a("#borderRadius").val()),
                zwWidth: parseInt(a("#zwWidth").val()),
                zwHeight: parseInt(a("#zwHeight").val()),
                zwShadow: parseInt(a("#zwShadow").val()),
                zwPadding: parseInt(a("#zwPadding").val()),
                zwBorderThickness: parseInt(a("#zwBorderThickness").val()),
                zwBorderColor: a("#zwBorderColor").val(),
                zwBorderRadius: parseInt(a("#zwBorderRadius").val()),
                lensFade: 1e3 * parseFloat(a("#lensFade").val()),
                zwFade: 1e3 * parseFloat(a("#zwFade").val()),
                tint: a("#tint").is(":checked"),
                tintColor: a("#tintColor").val(),
                tintOpacity: parseFloat(a("#tintOpacity").val())
            };
        switch (c(e), e.tintOpacity > 1 && (e.tintOpacity = 1), e.tintOpacity < 0 && (e.tintOpacity = 0), "zoom" === e.cursorType && (e.cursorType = "url(../images/cursor_type_zoom.svg) auto"), e.lensOverlay === !0 && (e.lensOverlay = "http://localhost/sb-test/wp-content/plugins/wp-image-zoooom-pro/assets/images/lens-overlay-1.png"), e.lensShape) {
            case "none":
                b = {
                    zoomType: "inner",
                    cursor: e.cursorType,
                    easingAmount: e.zwEasing
                };
                break;
            case "square":
            case "round":
                b = {
                    lensShape: e.lensShape,
                    zoomType: "lens",
                    lensSize: e.lensSize,
                    borderSize: e.borderThickness,
                    borderColour: e.borderColor,
                    cursor: e.cursorType,
                    lensFadeIn: e.lensFade,
                    lensFadeOut: e.lensFade
                }, e.tint === !0 && (b.tint = !0, b.tintColour = e.tintColor, b.tintOpacity = e.tintOpacity);
                break;
            case "zoom_window":
                b = {
                    lensShape: "square",
                    lensSize: e.lensSize,
                    lensBorderSize: e.borderThickness,
                    lensBorderColour: e.borderColor,
                    lensColour: e.lensColour,
                    lensOverlay: e.lensOverlay,
                    borderRadius: e.zwBorderRadius,
                    cursor: e.cursorType,
                    zoomWindowWidth: e.zwWidth,
                    zoomWindowHeight: e.zwHeight,
                    zoomWindowShadow: e.zwShadow,
                    borderSize: e.zwBorderThickness,
                    borderColour: e.zwBorderColor,
                    zoomWindowOffsetx: e.zwPadding,
                    lensFadeIn: e.lensFade,
                    lensFadeOut: e.lensFade,
                    zoomWindowFadeIn: e.zwFade,
                    zoomWindowFadeOut: e.zwFade,
                    easingAmount: e.zwEasing,
                    zoomWindowPosition: 1
                }, e.tint === !0 && (b.tint = !0, b.tintColour = e.tintColor, b.tintOpacity = e.tintOpacity), a("#demo_wrapper").css("text-align", "left")
        }
        a("#demo").image_zoom(b), a(window).bind("resize", function() {
            a(window).resize(function() {
                clearTimeout(window.resizeEvt), window.resizeEvt = setTimeout(function() {
                    a(".zoomContainer").remove(), a("#demo").image_zoom(b)
                }, 300)
            })
        }), d(e)
    }

    function c(b) {
        (isNaN(b.zwEasing) || b.zwEasing < 0 || b.zwEasing > 200) && (e("<b>Animation Easing Effect</b> accepts integers between 0 and 200. Your value was stripped to 12"), b.zwEasing = 12, a("#zwEasing").val("12")), (isNaN(b.lensSize) || b.lensSize < 20 || b.lensSize > 2e3) && (e("<b>Lens Size</b> accepts integers between 20 and 2000. Your value was reset to 200"), b.lensSize = 200, a("#lensSize").val("200")), (isNaN(b.borderThickness) || b.borderThickness < 0 || b.borderThickness > 200) && (e("<b>Border Thickness</b> accepts integers between 0 and 200. Your value was reset to 1"), b.borderThickness = 1, a("#borderThickness").val("1")), (isNaN(b.lensFade) || b.lensFade < 0 || b.lensFade > 1e4) && (e("<b>Fade Time</b> accepts integers between 0 and 10. Your value was reset to 1"), b.lensFade = 1, a("#lensFade").val("1")), (isNaN(b.tintOpacity) || b.tintOpacity < 0 || b.tintOpacity > 1) && (e("<b>Tint Opacity</b> accepts a number between 0 and 1. Your value was reset to 0.5"), b.tintOpacity = .5, a("#tintOpacity").val("0.5")), (isNaN(b.zwWidth) || b.zwWidth < 0 || b.zwWidth > 2e3) && (e("<b>Zoom Window Width</b> accepts a number between 0 and 2000. Your value was reset to 400"), b.zwWidth = 400, a("#zwWidth").val("400")), (isNaN(b.zwHeight) || b.zwHeight < 0 || b.zwHeight > 2e3) && (e("<b>Zoom Window Height</b> accepts a number between 0 and 2000. Your value was reset to 360"), b.zwHeight = 360, a("#zwHeight").val("360")), (isNaN(b.zwBorderThickness) || b.zwBorderThickness < 0 || b.zwBorderThickness > 200) && (e("<b>Border Thickness</b> accepts integers between 0 and 200. Your value was reset to 4"), b.zwBorderThickness = 4, a("#zwBorderThickness").val("4")), (isNaN(b.zwBorderRadius) || b.zwBorderRadius < 0 || b.zwBorderRadius > 500) && (e("<b>Rounded Corners</b> accepts integers between 0 and 500. Your value was reset to 0"), b.zwBorderRadius = 0, a("#zwBorderRadius").val("0")), (isNaN(b.zwFade) || b.zwFade < 0 || b.zwFade > 1e4) && (e("<b>Fade Time</b> accepts integers between 0 and 10. Your value was reset to 0"), b.zwFade = 0, a("#zwFade").val("0"))
    }

    function d(b) {
        switch (a("#tab_lens, #tab_zoom_window").removeClass("disabled"), a("#tab_lens a").attr("href", "#lens_settings"), a("#tab_zoom_window a").attr("href", "#zoom_window_settings"), a("#lensSize").removeAttr("disabled"), a("#lensColour").removeAttr("disabled"), a("#lensBgImage").removeAttr("disabled"), a("#tintColor").removeAttr("disabled"), a("#tintOpacity").removeAttr("disabled"), a("#lensColour").removeAttr("disabled"), a("#lensOverlay").removeAttr("disabled"), b.lensShape) {
            case "none":
                a("#tab_lens, #tab_zoom_window").addClass("disabled"), a("#tab_lens a").attr("href", ""), a("#tab_zoom_window a").attr("href", ""), a("#lensColour").attr("disabled", "disabled"), a("#lensBgImage").attr("disabled", "disabled");
                break;
            case "square":
            case "round":
                a("#tab_zoom_window").addClass("disabled"), a("#tab_zoom_window a").attr("href", ""), a("#lensColour").attr("disabled", "disabled"), a("#lensBgImage").attr("disabled", "disabled");
                break;
            case "zoom_window":
                a("#lensSize").attr("disabled", "disabled")
        }
        b.tint === !1 ? (a("#tintColor").attr("disabled", "disabled"), a("#tintOpacity").attr("disabled", "disabled")) : (a("#lensColour").attr("disabled", "disabled"), a("#lensOverlay").attr("disabled", "disabled"))
    }

    function e(b) {
        a("#alert_messages").html('<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>' + b + "</div>")
    }
    a('[data-toggle="tooltip"]').tooltip(), a("#demo").length > 0 && b(), a(".form-group input").change(b)
});
