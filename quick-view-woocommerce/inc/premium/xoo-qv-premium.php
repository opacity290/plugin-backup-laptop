<?php

//Exit if accessed directly
if(!defined('ABSPATH')){
	return;
}

?>

<div class="advanced-settings settings-tab premium-disabled" tab-class ="tab-2">
	<h3>Premium Settings</h3>
	<h2>General Options</h2>
		<table class="form-table">
			<tr>
				<th scope="row">Ajax add to cart</th>
				<td>
					<input type="checkbox" id="xoo-qv-premium" disabled checked>
					<label for="xoo-qv-premium">Add items to cart , without refreshing page.</label>
				</td>
			</tr>

			<tr>
				<th scope="row">Images Slideshow</th>
				<td>
					<input type="checkbox" id="xoo-qv-premium" disabled checked>
					<label for="xoo-qv-premium">Product images slideshow (Lightbox should be enabled)</label>
				</td>
			</tr>

			<tr>
				<th scope="row">Slideshow Speed</th>
				<td>
					<select disabled>
						<option>Fast</option>
						<option>Normal</option>
						<option>Slow</option>
					</select>
				</td>
			</tr>

			<tr>
				<th scope="row">Number of products</th>
				<td>
					<input type="checkbox" id="xoo-qv-premium" disabled checked>
					<label for="xoo-qv-premium">Shows the curent number of product out of total products.</label>
				</td>
			</tr>
		</table>
		<h2>Style Options</h2>
		<table class="form-table">
			<tr>
				<th scope="row" style="padding-top: 33px">Select Preloader</th>
				<td>
					<img src="<?php echo plugins_url('/preloaders.png',__FILE__); ?>">
				</td>
			</tr>

			<tr>
				<th scope="row">Preloader Color</th>
				<td>
					<input type="text" id="xoo-qv-premium" class='color-field' value="#db492b" disabled>
				</td>
			</tr>

			<tr>
				<th scope="row">Main Preloader Size</th>
				<td>
					<input type="number" id="xoo-qv-premium" disabled>
					<label for="xoo-qv-premium">Size in px (Default: 25px)</label>
				</td>
			</tr>

			<tr>
				<th scope="row">Secondary Preloader Size</th>
				<td>
					<input type="number" id="xoo-qv-premium" disabled>
					<label for="xoo-qv-premium">Switch Products preloader. Size in px (Default: 25px)</label>
				</td>
			</tr>
		</table>
	</div>