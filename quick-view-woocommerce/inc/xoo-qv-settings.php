<?php
//Exit if accessed directly
if(!defined('ABSPATH')){
	return;
}
?>
<h1>Quick View WooCommerce </h1>
<hr>
<?php settings_errors(); ?>
<div class="xoo-qv-main-settings">
<form method="POST" action="options.php" class="xoo-qv-form">
	<?php settings_fields('xoo-qv-group'); ?>
	<?php do_settings_sections('xoo_quickview'); ?>
	<?php 
	//Premium template
	include plugin_dir_path(__FILE__).'/premium/xoo-qv-premium.php' 
	?>
	<?php submit_button(); ?>
</form>
</div>
<div class="xoo-qv-sidebar"></div>
