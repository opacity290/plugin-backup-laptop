=== Quick View WooCommerce ===
Contributors: XootiX
Tags: WooCommerce Lightbox , WooCommerce quick view , Woocommerce fast view , Quick View , Lightbox
Requires at least: 3.0.1
Tested up to: 4.5.2
Stable tag: 1.2
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WooCommerce Quick View Enables customer to have a quick look of product without visiting product page.

== Description ==
[Live Demo](http://xootix.com)
Quick View WooCommerce is a simple and light weight plugin which allows users to get a quick look of products without opening the product page.Customers can navigate from one product to another using next and previous product button.


### Features And Options:
* Works for all types of products.
* Customizable Lightbox for product images & gallery.
* Next and previous product button for navigating from one product to another.
* Product Link button to visit the product page.
* Choose your own position for quick view button.
* Customizable Quick View modal.
* Option to enable/disable on mobile devices.


== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Click on Quick menu on the dashboard.

== Frequently Asked Questions ==

= How to setup? =

Everything is already setup.Just activate the plugin & you are done.

== Screenshots ==
1. The quick view button.
2. The quick view modal.
3. The quick view admin settings page.

== Changelog ==
= 1.2 =
* Fix - Added animation to lightbox preloader.

= 1.1 =
* Translation Available.
* Fixed bugs.

= 1.0 =
* Initial Public Release

