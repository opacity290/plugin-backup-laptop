jQuery(document).ready(function($){

// Lightbox
 function prettyPhotoLoad() {
    $("a.zoom").prettyPhoto({
        hook: 'data-rel',
        social_tools: false,
        theme: 'pp_woocommerce',
        horizontal_padding: 20,
        opacity: 0.8,
        deeplinking: false
    });
    $("a[data-rel^='prettyPhoto']").prettyPhoto({
    	show_title: xoo_qv_localize.prettyPhoto_title,
        hook: 'data-rel',
        social_tools: false,
        theme: 'pp_woocommerce',
        horizontal_padding: 20,
        opacity: 0.8,
        deeplinking: false,
    });

}

//Close window
$('.xoo-qv-panel').on('click','.xoo-qv-close',function(){
	$('.xoo-qv-opac').hide();
	$('.xoo-qv-panel').removeClass('xoo-qv-panel-active');
	$('.xoo-qv-modal').html('');
})

/*****    Ajax call on button click      *****/	
function xoo_qv_ajax(xoo_qv_id){
		$.ajax({
		url: xoo_qv_localize.adminurl,
		type: 'POST',
		data: {action: 'xoo_qv_ajax',
			   product_id: xoo_qv_id
			},
		success: function(response){
			$('.xoo-qv-modal').html(response);
			$('.xoo-qv-pl-active').removeClass('xoo-qv-pl-active');
			 prettyPhotoLoad();
		 	$('.xoo-qv-panel').find('.variations_form').wc_variation_form();
		 	$('.xoo-qv-panel .variations_form select').change();
			 
		},
	})
}

// Main Quickview Button
$(document).on('click','.xoo-qv-button',function(){
	$('.xoo-qv-opac').show();
	var xoo_qv_panel = $('.xoo-qv-panel');
	xoo_qv_panel.addClass('xoo-qv-panel-active');
	xoo_qv_panel.find('.xoo-qv-opl').addClass('xoo-qv-pl-active');
	var qv_id = $(this).attr('qv-id');
	xoo_qv_ajax(qv_id);
})

// Next Product
$('.xoo-qv-panel').on('click','.xoo-qv-nxt',function(){
	$('.xoo-qv-mpl').addClass('xoo-qv-pl-active');
	var qv_id = $(this).attr('qv-nxt-id');
	var product_nxt = $("[qv-id="+qv_id+"]").parents().next().find('.xoo-qv-button').attr('qv-id');
	var product_nxt_id;

	if(product_nxt === undefined){
		product_nxt_id = $('.xoo-qv-button:first').attr('qv-id');	
	}
	else{
		product_nxt_id = product_nxt;
	}

	xoo_qv_ajax(product_nxt_id);
})

//Previous Product
$('.xoo-qv-panel').on('click','.xoo-qv-prev',function(){
	$('.xoo-qv-mpl').addClass('xoo-qv-pl-active');
	var qv_id = $(this).attr('qv-prev-id');
	var product_prev = $('[qv-id='+qv_id+']').parents().prev().find('.xoo-qv-button').attr('qv-id');
	var product_prev_id;

	if(product_prev === undefined){
		product_prev_id = $('.xoo-qv-button:last').attr('qv-id');	
	}
	else{
		product_prev_id = product_prev;
	}

	xoo_qv_ajax(product_prev_id);
})

})
