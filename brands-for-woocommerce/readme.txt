=== Brands for WooCommerce ===
Plugin Name: Brands for WooCommerce
Contributors: dholovnia, berocket
Donate link: http://berocket.com/product/woocommerce-brands
Tags: product, product brand, brands for product, woocommerce brands, brand page, widget, plugin
Requires at least: 4.0
Tested up to: 4.6.1
Stable tag: 1.0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Brands for WooCommerce plugin allows you to create brands for products on your shop.

== Description ==

Brands for WooCommerce plugin allows you to create brands for products on your shop. Each brands has name, description and image.

= Features: =
* Custom image for brands
* Custom pages for brands
* Shortcode to display products by brand ID
* Widget with links for brand pages
* Display brand description and image on brand pages


= Additional Features in Paid Plugin: =
* Slider for brand links
* Additional customization
* Widget and shortcode for product brands list by name
* Shotcode to display brand image on product page
* Shortcode and widget to display brand information
* Option to display brand image on product pages


= Paid Plugin Link =
http://berocket.com/product/woocommerce-brands

= Demo =
http://woocommerce-brands.berocket.com/shop/

= Demo Description =
http://woocommerce-brands.berocket.com


= How It Works: =
*check installation*


== Installation ==
In WordPress:

1. In the admin panel you need to select Plugins-> Add New-> In the upper left corner next to the inscription Add Plugins click Upload Plugin, and then select the downloaded beforehand Fail and click Install Now. / In the admin panel you need to select Plugins-> Add New-> via find the desired plugin and click Install Now.
2. In the Admin panel, select Plugins and click on the plugin called Activate button.


To install manually:

1. It should be a folder with a plug placed in the directory wp-content-> plugins.
2. In the admin panel, select Plugins and click under the plug-in name in the Activate button.


== Frequently Asked Questions ==

---

== Screenshots ==

---

== Changelog ==

= 1.0.2 =
* Better support for PHP 5.2

= 1.0.1 =
* First public version
