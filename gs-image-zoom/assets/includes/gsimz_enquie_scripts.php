<?php

function gsmiz_imagezoom_get_option( $option, $section, $default = '' ) {

		$options = get_option( $section );

		if ( isset( $options[$option] ) ) {
				return $options[$option];
		}

		return $default;
}

$prefix = 'gsmiz_';


//-------------- Include js files---------------
function gs_imz_enq_scripts() {
	 $prefix = 'gsmiz_';


		 	wp_enqueue_script( 'gsimz_zoomjs', GSIMZ_FILES_URI . '/js/jquery.imzoom.min.js', array( 'jquery' ), GSIMZ_VERSION, true );
 			wp_enqueue_script( 'gsimz_custmzoomjs', GSIMZ_FILES_URI . '/js/gsimz.custom-function.js', array( 'gsimz_zoomjs' ), GSIMZ_VERSION, true );


		// getting the options
    $lensType = gsmiz_imagezoom_get_option($prefix.'lens_type', $prefix.'settings', 'square' );
    $lensSize = gsmiz_imagezoom_get_option($prefix.'lensSize', $prefix.'settings', '' );
    $widthz = gsmiz_imagezoom_get_option($prefix.'widthz', $prefix.'settings', '400' );
    $heightz = gsmiz_imagezoom_get_option($prefix.'heightz', $prefix.'settings', '400' );
    $tinzActive = (gsmiz_imagezoom_get_option($prefix.'tinzActive', $prefix.'settings', 'false' ) == 'false') ? false : true;
    $tinz = gsmiz_imagezoom_get_option($prefix.'tinz', $prefix.'settings', '' );
    $tinz_opacity = gsmiz_imagezoom_get_option($prefix.'tinz_opacity', $prefix.'settings', '0.4' );
    $opacityz = gsmiz_imagezoom_get_option($prefix.'opacityz', $prefix.'settings', '0.4' );
    $easing = (gsmiz_imagezoom_get_option($prefix.'easing', $prefix.'settings', false ) == 'false') ? false : true ;
    $zoomborder = gsmiz_imagezoom_get_option($prefix.'zoomborder', $prefix.'settings', 4 );
    $zoomBorderColor = gsmiz_imagezoom_get_option($prefix.'zoomBorderColor', $prefix.'settings', '#888' );
    $ScrollToZoom = (gsmiz_imagezoom_get_option($prefix.'ScrollToZoom', $prefix.'settings', 'false' ) == 'false') ? false : true;
    $zoomCursor = gsmiz_imagezoom_get_option($prefix.'zoomCursor', $prefix.'settings', 'default' );
    $easing_duration = gsmiz_imagezoom_get_option($prefix.'easing_duration', $prefix.'settings', 2000 );
    $gsmiz_settings_tags = array(
        'lensType' => $lensType,
        'lensSize' => $lensSize,
        'widthz' => $widthz,
        'heightz' => $heightz,
        'tinzActive' => $tinzActive,
        'tinz' => $tinz,
        'tinzOpacity' => $tinz_opacity,
        'opacityz' => $opacityz,
        'easing' => $easing,
        'zoomborder' => $zoomborder,
        'zoomBorderColor' => $zoomBorderColor,
        'ScrollToZoom' => $ScrollToZoom,
        'getzoomCursor' => $zoomCursor,
        'easing_duration' => $easing_duration,
    );
    // sending the options to the js file
    wp_localize_script( 'gsimz_custmzoomjs', 'settings', $gsmiz_settings_tags );

}
if ( gsmiz_imagezoom_get_option($prefix.'Status', $prefix.'settings', 'no' ) == 'yes') {
	add_action( 'wp_enqueue_scripts', 'gs_imz_enq_scripts' );
}
