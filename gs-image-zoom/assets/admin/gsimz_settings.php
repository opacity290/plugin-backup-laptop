<?php

/**
 * WordPress settings API
 *
 * @author Tareq Hasan
 */
if ( !class_exists('gsimz_Settings_API_feilds' ) ):
class gsimz_Settings_API_feilds {

    private $settings_api;
    public $prefix = 'gsmiz_';

    function __construct() {
        $this->settings_api = new gsimz_Settings_API;

        add_action( 'admin_init', array($this, 'admin_init') );
        add_action( 'admin_menu', array($this, 'admin_menu') );
    }

    function admin_init() {

        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );

        //initialize settings
        $this->settings_api->admin_init();
    }

    function admin_menu() {
        add_options_page( 'GS Image zoom', 'GS Image zoom', 'delete_posts', 'gsimz_settings', array($this, 'plugin_page') );
    }

    function get_settings_sections() {
        $sections = array(
            array(
                'id' => $this->prefix.'settings',
                'title' => __( 'GS Image zoom Settings', 'gsimz' )
            ),
        );
        return $sections;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_settings_fields() {
        $settings_fields = array(
            ''.$this->prefix.'settings' => array(
                array(
                    'name'    => $this->prefix.'Status',
                    'label'   => __( 'Plugin status', 'gsimz' ),
                    'desc'    => __( 'Select your plugin status, comming soon page will redirect if the status Active', 'gsimz' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => array(
                        'yes' => 'Active',
                        'no'  => 'Deactive'
                    )
                ),
                array(
                    'name'    => $this->prefix.'lens_type',
                    'label'   => __( 'Lens type', 'gsimz' ),
                    'desc'    => __( 'Choose Lens type', 'gsimz' ),
                    'type'    => 'radio',
                    'default' => 'square',
                    'options' => array(
                        'square' => 'square',
                        'round' => 'round',
                        'outer_lens' => 'outer_lens',
                        'inner' => 'inner',
                    )
                ),
                array(
                    'name'    => $this->prefix.'lensSize',
                    'label'   => __( 'Size of the zoom', 'gsimz' ),
                    'desc'    => __( 'Choose Size of the zoom', 'gsimz' ),
                    'type'    => 'number',
                    'default' => '200',
                    'options' => array(
                        'button_label' => 'Choose width of the zoom'
                    )
                ),
                array(
                    'name'    => $this->prefix.'ScrollToZoom',
                    'label'   => __( 'zoom Scroll ?', 'gsimz' ),
                    'desc'    => __( 'Choose zoomScroll on or off', 'gsimz' ),
                    'type'    => 'Select',
                    'default' => 'false',
                    'options' => array(
                        'true' => 'true',
                        'false' => 'false',
                    )
                ),
                array(
                    'name'    => $this->prefix.'zoomCursor',
                    'label'   => __( 'zoom cursor', 'gsimz' ),
                    'desc'    => __( 'Choose zoom Cursor', 'gsimz' ),
                    'type'    => 'Select',
                    'default' => 'default',
                    'options' => array(
                        'default' => 'default',
                        'cursor' => 'cursor',
                        'crosshair' => 'crosshair',
                    )
                ),
                array(
                    'name'    => $this->prefix.'zoomborder',
                    'label'   => __( 'zoomborder', 'gsimz' ),
                    'desc'    => __( 'Choose zoomborder ', 'gsimz' ),
                    'type'    => 'number',
                    'default' => '4',
                ),
                array(
                    'name'    => $this->prefix.'zoomBorderColor',
                    'label'   => __( 'zoom Border Color', 'gsimz' ),
                    'desc'    => __( 'Choose zoom Border Color', 'gsimz' ),
                    'type'    => 'color',
                    'default' => '#888',
                ),
                array(
                    'name'    => $this->prefix.'widthz',
                    'label'   => __( 'width of the zoom', 'gsimz' ),
                    'desc'    => __( 'Choose width of the zoom', 'gsimz' ),
                    'type'    => 'number',
                    'default' => '',
                    'options' => array(
                        'button_label' => 'Choose width of the zoom'
                    )
                ),
                array(
                    'name'    => $this->prefix.'heightz',
                    'label'   => __( 'height of the zoom', 'gsimz' ),
                    'desc'    => __( 'Choose height of the zoom', 'gsimz' ),
                    'type'    => 'number',
                    'default' => '',
                    'options' => array(
                        'button_label' => 'Choose height of the zoom'
                    )
                ),
                array(
                    'name'              => $this->prefix.'tinzActive',
                    'label'             => __( 'Active tin color', 'gsimz' ),
                    'desc'              => __( 'Active tin color  mask when its hover Note: its work well when lens in outer_lens mode option ', 'gsimz' ),
                    'type'              => 'radio',
                    'default'           => 'false',
                    'options' => array(
                        'true' => 'true',
                        'false' => 'false',
                    )
                ),
                array(
                    'name'              => $this->prefix.'tinz',
                    'label'             => __( 'tin color', 'gsimz' ),
                    'desc'              => __( 'Make a zoom color mask when its hover', 'gsimz' ),
                    'type'              => 'color',
                    'default'           => '',
                ),
                array(
                    'name'              => $this->prefix.'lens_color',
                    'label'             => __( 'lens color ', 'gsimz' ),
                    'desc'              => __( 'Make a zoom color mask when its hover', 'gsimz' ),
                    'type'              => 'color',
                    'default'           => 'white',
                ),

                array(
                    'name'              => $this->prefix.'tinz_opacity',
                    'label'             => __( 'tin opacity', 'gsimz' ),
                    'desc'              => __( 'Make a zoom color mask when its hover', 'gsimz' ),
                    'type'              => 'text',
                    'default'           => '0.4',
                ),
                array(
                    'name'    => $this->prefix.'opacityz',
                    'label'   => __( 'set lens opacity', 'gsimz' ),
                    'desc'    => __( 'set lens opacity of the zoom', 'gsimz' ),
                    'type'    => 'text',
                    'default' => '0.4'
                ),
                array(
                    'name'    => $this->prefix.'easing',
                    'label'   => __( 'easing effect', 'gsimz' ),
                    'desc'    => __( 'show your zoom more smoothly', 'gsimz' ),
                    'type'    => 'radio',
                    'default' => 'false',
                    'options' => array(
                        'true' => 'true',
                        'false' => 'false',
                    )
                ),
                array(
                    'name'    => $this->prefix.'easing_duration',
                    'label'   => __( 'easing duration effect', 'gsimz' ),
                    'desc'    => __( 'show your zoom more smoothly', 'gsimz' ),
                    'type'    => 'number',
                    'default' => '2000',
                ),
            ),
        );

        return $settings_fields;
    }

    function plugin_page() {
        echo '<div class="wrap">';

        $this->settings_api->show_navigation();
        $this->settings_api->show_forms();

        echo '</div>';
    }

    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function get_pages() {
        $pages = get_pages();
        $pages_options = array();
        if ( $pages ) {
            foreach ($pages as $page) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }

        return $pages_options;
    }

}
endif;
