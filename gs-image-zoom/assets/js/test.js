jQuery(document).ready(function($) {

    var lensType = gsmiz_settings.lensType;

    $(".attachment-shop_single").elevateZoom({

      zoomType: "lens",
      lensShape: lensType.lensType,
      lensSize: 200,
      scrollZoom: true
    });

});



  // jQuery(".attachment-shop_single").elevateZoom({
  //
  //   var lensType = gsmiz_settings.lensType;
  //
  //   zoomType: "lens",
  //   lensShape: var lensType ,
  //   lensSize: 200,
  //   scrollZoom: true
  // });

  $("#zoom_01").elevateZoom({scrollZoom : true});

  $("#zoom_02").elevateZoom({
    zoomType: "inner",
    cursor: "crosshair",
    scrollZoom : true
  });

  $("#zoom_03").elevateZoom({
    zoomType: "lens",
    lensShape: "round",
    lensSize: 200,
    scrollZoom: true
  });
