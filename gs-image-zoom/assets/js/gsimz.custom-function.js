jQuery(document).ready(function($) {

	var lensType = settings.lensType;//done
	var lensSize = settings.lensSize;//done
	var widthz = settings.widthz;//done
	var heightz = settings.heightz;//done
	var tinzActive = settings.tinzActive;
	var tinz = settings.tinz;
	var opacityz = settings.opacityz;
	var tinzOpacity = settings.tinzOpacity;
	var easing = settings.easing;//done
	var ScrollToZoom = settings.ScrollToZoom;//done
	var getzoomCursor = settings.getzoomCursor;//done
	var zoomborder = settings.zoomborder;//done
	var zoomBorderColor = settings.zoomBorderColor;//done
	var easing_duration = settings.easing_duration;//done
	// alert(ScrollToZoom);

	if (lensType == 'round' || 'square') {

		$(".attachment-shop_single").elevateZoom({

      zoomType: "lens",
      lensShape: lensType,
      lensSize: lensSize,
			scrollZoom: ScrollToZoom,
			easing: easing,
			easingDuration: easing_duration,
			lensOpacity: opacityz,
			borderSize: zoomborder,
			borderColour: zoomBorderColor ,
			lensBorder: 1,
			cursor: getzoomCursor,
			tint: tinzActive,
			tintColour: tinz,
			tintOpacity: tinzOpacity,
    });
	}
	 if (lensType == 'inner') {
		$(".attachment-shop_single").elevateZoom({
      zoomType: "inner",
      scrollZoom: ScrollToZoom,
			easing: easing,
			easingDuration: easing_duration,
			borderSize: zoomborder,
			borderColour: zoomBorderColor ,
			lensBorder: 1,
			cursor: getzoomCursor,
			tint: tinzActive,
			tintColour: tinz,
			tintOpacity: tinzOpacity,
    });
	}
	if (lensType == 'outer_lens') {
		$(".attachment-shop_single").elevateZoom({
			zoomType: "window",
			lensSize: lensSize,
      scrollZoom: ScrollToZoom,
			easing: easing,
			easingDuration: easing_duration,
			zoomWindowWidth: widthz,
			zoomWindowHeight: heightz,
			borderSize: zoomborder,
			borderColour: zoomBorderColor ,
			lensBorder: 1,
			cursor: getzoomCursor,
			tint: tinzActive,
			tintColour: tinz,
			tintOpacity: tinzOpacity,

    });
	}


});
