<?php
/**
 *
 * @package   GS_image_zoom
 * @author    Golam Samdani <samdani1997@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.gsamdani.com
 * @copyright 2014 Golam Samdani
 *
 * @wordpress-plugin
 * Plugin Name:			GS image zoom
 * Plugin URI:			http://www.gsamdani.com/wordpress-plugins
 * Description:       	Best Responsive Logo slider to display partners, clients or sponsors Logo on Wordpress site. Display anywhere at your site using shortcode like [gs_logo] Check more shortcode examples and documention at <a href="http://logo.gsamdani.com">GS Logo Slider Docs</a>
 * Version:           	1.0.0
 * Author:       		Golam Samdani
 * Author URI:       	http://www.gsamdani.com
 * Text Domain:       	gsimz
 * License:           	GPL-2.0+
 * License URI:       	http://www.gnu.org/licenses/gpl-2.0.txt
 */

if( ! defined( 'GSIMZ_HACK_MSG' ) ) define( 'GSIMZ_HACK_MSG', __( 'Sorry cowboy! This is not your place', 'gsl' ) );

/**
 * Protect direct access
 */
if ( ! defined( 'ABSPATH' ) ) die( GSIMZ_HACK_MSG );
/**
 * Defining constants
 */
if( ! defined( 'GSIMZ_VERSION' ) ) define( 'GSIMZ_VERSION', '1.0.0' );
if( ! defined( 'GSIMZ_MENU_POSITION' ) ) define( 'GSIMZ_MENU_POSITION', 5 );
if( ! defined( 'GSIMZ_PLUGIN_DIR' ) ) define( 'GSIMZ_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
if( ! defined( 'GSIMZ_PLUGIN_URI' ) ) define( 'GSIMZ_PLUGIN_URI', plugins_url( '', __FILE__ ) );
if( ! defined( 'GSIMZ_FILES_DIR' ) ) define( 'GSIMZ_FILES_DIR', GSIMZ_PLUGIN_DIR . 'assets' );
if( ! defined( 'GSIMZ_FILES_URI' ) ) define( 'GSIMZ_FILES_URI', GSIMZ_PLUGIN_URI . '/assets' );


require_once GSIMZ_FILES_DIR . '/admin/gsimz_class.settings-api.php';
require_once GSIMZ_FILES_DIR . '/admin/gsimz_settings.php';
new gsimz_Settings_API_feilds();
// require_once GSIMZ_FILES_DIR . '/includes/render_data_functions.php';
require_once GSIMZ_FILES_DIR . '/includes/gsimz_enquie_scripts.php';
// new GSMIZ_IMAGEZOOM();
