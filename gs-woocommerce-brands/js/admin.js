var gs_saved_timeout;
var gs_savin_ajax = false;
function brand_widget_init() {
    jQuery('.colorpicker_field_brand').each(function (i,o){
        jQuery(o).css('backgroundColor', '#'+jQuery(o).data('color'));
        jQuery(o).colpick({
            layout: 'hex',
            submit: 0,
            color: '#'+jQuery(o).data('color'),
            onChange: function(hsb,hex,rgb,el,bySetColor) {
                jQuery(el).css('backgroundColor', '#'+hex).next().val(hex).trigger('change');
            }
        })
    });
}
(function ($){
    $(document).ready( function () {
        brand_widget_init();
        $('.product_brand_submit_form').submit( function(event) {
            event.preventDefault();
            if( !gs_savin_ajax ) {
                gs_savin_ajax = true;
                var form_data = $(this).serialize();
                form_data = 'action=gs_product_brand_settings_save&'+form_data;
                var url = ajaxurl;
                clearTimeout(gs_saved_timeout);
                destroy_br_saved();
                $('body').append('<span class="gs_saved gs_saving"><i class="fa fa-refresh fa-spin"></i></span>');
                $.post(url, form_data, function (data) {
                    if($('.gs_saved').length > 0) {
                        $('.gs_saved').removeClass('gs_saving').find('.fa').removeClass('fa-spin').removeClass('fa-refresh').addClass('fa-check');
                    } else {
                        $('body').append('<span class="gs_saved"><i class="fa fa-check"></i></span>');
                    }
                    gs_saved_timeout = setTimeout( function(){destroy_br_saved();}, 5000 );
                    gs_savin_ajax = false;
                }, 'json').fail(function() {
                    if($('.gs_saved').length > 0) {
                        $('.gs_saved').removeClass('gs_saving').addClass('gs_not_saved').find('.fa').removeClass('fa-spin').removeClass('fa-refresh').addClass('fa-times');
                    } else {
                        $('body').append('<span class="gs_saved gs_not_saved"><i class="fa fa-times"></i></span>');
                    }
                    br_saved_timeout = setTimeout( function(){destroy_br_saved();}, 5000 );
                    $('.gs_save_error').html(data.responseText);
                    br_savin_ajax = false;
                });
            }
        });
        function destroy_gs_saved() {
            $('.gs_saved').addClass('gs_saved_remove');
            var $get = $('.gs_saved');
            setTimeout( function(){$get.remove();}, 200 );
        }
        $('.gs_settings .nav-tab').click(function(event) {
            event.preventDefault();
            $('.nav-tab-active').removeClass('nav-tab-active');
            $('.nav-block-active').removeClass('nav-block-active');
            $(this).addClass('nav-tab-active');
            $('.'+$(this).data('block')+'-block').addClass('nav-block-active');
        });
        $(window).on('keydown', function(event) {
            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    $('.product_brand_submit_form').submit();
                    break;
                }
            }
        });

        $(document).on('click', '.gs_brands_image .gswcbr_aapf_upload_icon', function(e) {
            e.preventDefault();
            $p = $(this);
            var custom_uploader = wp.media({
                title: 'Select custom Icon',
                button: {
                    text: 'Set Icon'
                },
                multiple: false
            }).on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $p.prevAll(".gswcbr_aapf_selected_icon_show").html('<i class="fa"><image src="'+attachment.url+'" alt=""></i>');
                $p.prevAll(".gswcbr_aapf_icon_text_value").val(attachment.url);
            }).open();
        });
        $(document).on('click', '.gs_brands_image .gswcbr_aapf_remove_icon',function(event) {
            event.preventDefault();
            $(this).prevAll(".gswcbr_aapf_icon_text_value").val("");
            $(this).prevAll(".gswcbr_aapf_selected_icon_show").html("");
        });

        $(document).on('click', '.theme_default', function (event) {
            event.preventDefault();
            $(this).prev().prev().css('backgroundColor', '#000000').colpickSetColor('#000000');
            $(this).prev().val('');
        });
    });
})(jQuery);
