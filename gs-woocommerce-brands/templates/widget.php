<?php if( $title ) echo $args['before_title'].$title.$args['after_title'];
$args = array(
    'hide_empty' => ( @ $hide_empty ? true : false ),
);
if( ! empty( $count ) ) {
    $args['number'] = $count;
}
if( ! empty( $orderby ) ) {
    $args['orderby'] = $orderby;
}
$terms = get_terms( 'gswcbr_brand', $args );
foreach($terms as $term) {
    echo '<div class="gs_widget_brand_element">';
    if( $use_image ) {
        $image 	= get_woocommerce_term_meta( $term->term_id, 'brand_image_url', true );
        if( ! empty($image) ) {
            echo '<a href="', get_term_link( $term->term_id, 'gswcbr_brand' ), '"><img style="max-height:', $imgh, 'px;" src="', $image, '" alt="', $term->name, '"></a>';
        }
    }
    if( $use_name ) {
        echo '<a href="', get_term_link( $term->term_id, 'gswcbr_brand' ), '">', $term->name, '</a>';
    }
    echo '</div>';
}
?>
